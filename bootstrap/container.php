<?php
//IOC container
use App\Application\Contracts\Provider\AttributesProviderInterface;
use App\Application\Contracts\Provider\AttributeValueProviderInterface;
use App\Application\Contracts\Service\LoginRequestServiceInterface;
use App\Application\Contracts\Service\RegisterRequestServiceInterface;
use App\Application\Contracts\Service\UserRequestServiceInterface;
use App\Application\Provider\AttributesProvider;
use App\Application\Provider\AttributeValueProvider;
use App\Application\Service\LoginRequest;
use App\Application\Service\RegisterRequest;
use App\Application\Service\UserRequestService;
use App\Domain\Contracts\Repository\Mysql\AttributeRepositoryInterface;
use App\Domain\Contracts\Repository\Mysql\AttributeValuesRepositoryInterface;
use App\Domain\Contracts\Repository\Mysql\UserRepositoryInterface;
use App\Infrastructure\Repository\Mysql\AttributeRepository;
use App\Infrastructure\Repository\Mysql\AttributeValuesRepository;
use App\Infrastructure\Repository\Mysql\UserRepository;
use function DI\autowire;

$builder = new DI\ContainerBuilder();
$builder->useAnnotations(false);
$builder->useAutowiring(true);
$container = $builder->build();

$container->set(UserRepositoryInterface::class, autowire(UserRepository::class));
$container->set(LoginRequestServiceInterface::class, autowire(LoginRequest::class));
$container->set(RegisterRequestServiceInterface::class, autowire(RegisterRequest::class));
$container->set(AttributeRepositoryInterface::class, autowire(AttributeRepository::class));
$container->set(UserRequestServiceInterface::class, autowire(UserRequestService::class));
$container->set(AttributesProviderInterface::class, autowire(AttributesProvider::class));
$container->set(AttributeValueProviderInterface::class, autowire(AttributeValueProvider::class));
$container->set(AttributeValuesRepositoryInterface::class, autowire(AttributeValuesRepository::class));


