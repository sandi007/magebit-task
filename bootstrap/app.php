<?php
//app start
use App\Utility\Config;
use DI\Bridge\Slim\Bridge;
require __DIR__.'/../bootstrap/container.php';
$app = Bridge::create($container);
$app->addErrorMiddleware(true, true, true);
$app->setBasePath(Config::get('app.BASE_PATH'));

require __DIR__.'/../routes/web.php';

$app->run();