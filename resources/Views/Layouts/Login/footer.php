</div>
</section>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    var ajax_prevent_login = false;
    var ajax_prevent_register = false;

    $("#login_submit").on("click", function (e) {
        e.preventDefault();
        let email = $("#email").val();
        let password = $("#password").val();

        if (!ajax_prevent_login) {
            ajax_prevent_login = true;
            $.ajax({
                cache: false,
                method: 'POST',
                url: '<?php echo $this->webroot(); ?>/login_request',
                data: {'email': email, 'password': password},
                success: function (response) {
                    if (response.success) {
                        $("#feedback_message").html('<span style="color: green" >' + response.message + '</span>');
                        window.location ='<?php echo $this->webroot(); ?>/index';
                    }
                    if (!response.success) {
                        $("#feedback_message").html('<span style="color: red" >' + response.message + '</span>');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                }
            }).done(function () {
                ajax_prevent_login = false;
            });
        }
    });

    $("#register_submit").on("click", function (e) {
        e.preventDefault();
        let name = $("#name").val();
        let email = $("#register_email").val();
        let password = $("#register_password").val();

        if (!ajax_prevent_register) {

            ajax_prevent_register = true;
            $.ajax({
                cache: false,
                method: 'POST',
                url: '<?php echo $this->webroot(); ?>/register_request',
                data: {'name': name, 'email': email, 'password': password},
                success: function (response) {
                    if (response.success) {
                        $("#feedback_message_register").html('<span style="color: green" >' + response.message + '</span>');
                    }
                    if (!response.success)
                    {
                        $("#feedback_message_register").html('<span style="color: red" >' + response.message + '</span>');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(JSON.stringify(jqXHR));
                }
            }).done(function () {
                ajax_prevent_register = false;
            });
        }
    });
</script>
<script src="<?php echo $this->webroot() ?>/resources/js/main.js"></script>
<script src="<?php echo $this->webroot() ?>/resources/js/script.js"></script>
</body>
</html>