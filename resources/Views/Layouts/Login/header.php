<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="apple-touch-icon" type="image/png" href="<?php echo $this->webroot() ?>/resources/img/logo.png"/>
    <meta name="apple-mobile-web-app-title" content="CodePen">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->webroot() ?>/resources/img/logo.png"/>
    <link rel="mask-icon" type="" href="<?php echo $this->webroot() ?>/resources/img/logo.png"/>
    <title>Animated Login and Sign up</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300, 400, 500" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300, 400, 500" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css'>
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="<?php echo $this->webroot() ?>/resources/css/style.css">
    <script>
        window.console = window.console || function (t) {
        };
    </script>
</head>
<body translate="no">
<div id="bkg">
    <section class="user">
        <div class="user_options-container">
