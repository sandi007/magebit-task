<section class="cnt">
    <div id="div1" class="targetDiv" style="display: block;">
        <h2>Edit attributes</h2>
        <p>
            <?php
            $this->renderFeedbackMessage();
            ?>
        </p>

        <form id="_form" action="<?php echo $this->webroot(); ?>/users/update" method="post">
            <input type="hidden" name="id" value="<?php echo $this->user_attributes_with_value->user->id ?>">

            <div class="container">
                <label for="name"><b>Name</b></label>
                <input type="text" placeholder="Enter Name" name="name"
                       value="<?php echo $this->user_attributes_with_value->user->name ?>">

                <label for="email"><b>Email</b></label>
                <input type="text" placeholder="Enter Email" name="email"
                       value="<?php echo $this->user_attributes_with_value->user->email ?>">

                <label for="password"><b>Password</b></label>
                <input type="password" placeholder="Enter Password" name="password">
                <?php
                if (isset($this->user_attributes_with_value->attributes))
                    $this->generateAttributeFormElements($this->user_attributes_with_value->attributes);
                ?>
                <button type="submit">Edit</button>
            </div>
        </form>
    </div>
</section>

