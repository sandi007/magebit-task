<section class="cnt">
    <div id="div1" class="targetDiv" style="display: block;">
        <h2>User table data</h2>
        <?php
        $this->renderFeedbackMessage();
        ?>
        <table id="dataTable">
            <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($this->users)): ?>
                <?php foreach ($this->users as $user): ?>
                    <tr>
                        <td><?php echo $user->name; ?></td>
                        <td><?php echo $user->email; ?></td>
                        <td>
                            <button onclick="location.href='<?php echo $this->webroot(); ?>/users/edit/<?php echo $user->id ?>'">
                                Edit
                            </button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</section>