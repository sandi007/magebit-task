<section class="cnt">
    <div id="div1" class="targetDiv" style="display: block;">
        <h2>Add attributes</h2>
        <p>
            <?php
            $this->renderFeedbackMessage();
            ?>
        </p>
        <form id="_form" action="<?php echo $this->webroot(); ?>/attributes/store" method="post">
            <label for="name">Attribute Name:</label><br>
            <input type="text" id="name" name="name" value=""></br>
            <label for="name">Attribute Label:</label><br>
            <input type="text" id="label" name="label" value=""></br>
            <label for="type">Attribute Type:</label><br>
            <select name="type">
                <option value=""></option>
                <?php if (!empty($this->data["attributes_types"])):
                    foreach ($this->data["attributes_types"] as $type):
                        ?>
                        <option value="<?php echo $type ?>"><?php echo $type ?></option>
                    <?php endforeach; endif; ?>
            </select><br>
            <label for="table_name">Table Name:</label><br>
            <select name="table_name">
                <option value=""></option>
                <?php if (!empty($this->data["attribute_tables"])):
                    foreach ($this->data["attribute_tables"] as $table):
                        ?>
                        <option value="<?php echo $table ?>"><?php echo $table ?></option>
                    <?php endforeach; endif; ?>
            </select><br>
            <input type="radio" id="active" name="status" value="1">
            <label for="active">Active</label><br>
            <input type="radio" id="inactive" name="status" value="0">
            <label for="inactive">Inactive</label><br>
            <input type="submit" value="Submit">
        </form>
    </div>
</section>

