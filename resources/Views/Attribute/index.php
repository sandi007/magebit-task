<section class="cnt">
    <div id="div1" class="targetDiv" style="display: block;">
        <h2>Attribute table data</h2>
        <p>
            <?php
            $this->renderFeedbackMessage();
            ?>
        </p>


        <table id="dataTable">
            <thead>
            <tr>
                <th>Name</th>
                <th>Label</th>
                <th>Type</th>
                <th>Table Name</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($this->data)): ?>
                <?php foreach ($this->data as $atttribute): ?>
                    <tr>
                        <td><?php echo $atttribute->name; ?></td>
                        <td><?php echo $atttribute->label; ?></td>
                        <td><?php echo $atttribute->type; ?></td>
                        <td><?php echo $atttribute->table_name; ?></td>
                        <td><?php echo ($atttribute->status == 1) ? 'Active' : 'Inactive'; ?></td>
                        <td>
                            <button onclick="location.href='<?php echo $this->webroot(); ?>/attributes/edit/<?php echo $atttribute->id ?>'">
                                Edit
                            </button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</section>
