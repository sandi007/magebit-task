<section class="cnt">
    <div id="div1" class="targetDiv" style="display: block;">
        <h2>Edit attributes</h2>
        <p>
            <?php
            $this->renderFeedbackMessage();
            ?>
        </p>
        <form id="_form" action="<?php echo $this->webroot(); ?>/attributes/update" method="post">
            <input type="hidden" name="id" value="<?php echo $this->data["attribute"]->id; ?>"/>
            <label for="name">Attribute Name:</label><br>
            <input type="text" id="name" name="name" value="<?php echo $this->data["attribute"]->name ?>"></br>
            <label for="name">Attribute Label:</label><br>
            <input type="text" id="label" name="label" value="<?php echo $this->data["attribute"]->label ?>"></br>
            <label for="type">Attribute Type:</label><br>
            <select name="type">
                <option value=""></option>
                <?php if (!empty($this->data["attributes_types"])):
                    foreach ($this->data["attributes_types"] as $type):
                        $selected_attribute_type = ($this->data["attribute"]->type === $type) ? "selected" : "";
                        ?>
                        <option value="<?php echo $type ?>" <?php echo $selected_attribute_type; ?>><?php echo $type ?></option>
                    <?php endforeach; endif; ?>
            </select><br>
            <label for="table_name">Table Name:</label><br>
            <select name="table_name">
                <option value=""></option>
                <?php if (!empty($this->data["attribute_tables"])):
                    foreach ($this->data["attribute_tables"] as $table):
                        $selected_attribute_table = ($this->data["attribute"]->table_name === $table) ? "selected" : "";
                        ?>
                        <option value="<?php echo $table ?>" <?php echo $selected_attribute_table; ?>><?php echo $table ?></option>
                    <?php endforeach; endif; ?>
            </select><br>
            <input type="radio" id="active"
                   name="status" <?php echo ($this->data["attribute"]->status == 1) ? "checked" : ""; ?> value="1">
            <label for="active">Active</label><br>
            <input type="radio" id="inactive"
                   name="status" <?php echo ($this->data["attribute"]->status == 0) ? "checked" : ""; ?> value="0">
            <label for="inactive">Inactive</label><br>
            <input type="submit" value="Submit">
        </form>
    </div>
</section>
