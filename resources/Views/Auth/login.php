<div class="user_options-text">
    <div class="user_options-unregistered">
        <h2 class="user_unregistered-title">Don't have an account?</h2>

        <div style="width:40px;padding:10px 0px 20px 0px">
            <div id="borderLeft"></div>
        </div>
        <p class="user_unregistered-text">Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever
            street art fap whatever street art fap whatever street art fap whatever street art .</p>
        <button class="user_unregistered-signup" id="signup-button">Sign up</button>
    </div>
    <div class="user_options-registered">
        <h2 class="user_registered-title">Have an account?</h2>
        <div style="width:40px;padding:10px 0px 20px 0px">
            <div id="borderLeft"></div>
        </div>
        <p class="user_registered-text">Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever
            street art fap.</p>
        <button class="user_registered-login" id="login-button">Login</button>
    </div>
</div>
<div class="user_options-forms" id="user_options-forms">
    <div class="user_forms-login">
        <h2 class="forms_title">Login <label for="login-mail" style="float:right"><img
                        src="<?php echo $this->webroot() ?>/resources/img/logo.png" style="width:35px"></label></h2>
        <div style="width:40px;margin-top:-20px;padding:7px 0px 15px 0px">
            <div id="borderLeft"></div>
        </div>
        <form class="forms_form">
            <fieldset class="forms_fieldset">
                <p id="feedback_message"></p>

                <table>
                    <tr>
                        <td>
                            <div class="forms_field">
                                <input type="email" placeholder="Email" class="forms_field-input" name="email"
                                       id="email"/>
                        </td>
                        <td><label for="login-mail"><i class="fa fa-envelope-o"></i></label>
    </div>
    </td></tr>


    <tr>
        <td>
            <div class="forms_field">
                <input type="password" placeholder="Password" class="forms_field-input" name="password" id="password"/>
        </td>
        <td><label for="login-mail"><i class="fa fa-user-o"></i></label>
</div></td></tr></table>

</table>

</fieldset>
<div class="forms_buttons">

    <input type="button" value="Log In" class="forms_buttons-action" name="login" id="login_submit">
    <button type="button" class="forms_buttons-forgot" onclick="return">Forgot?</button>
</div>
</form>
</div>
<div class="user_forms-signup">
    <h2 class="forms_title">Sign Up <label for="login-mail" style="float:right"><img
                    src="<?php echo $this->webroot() ?>/resources/img/logo.png" style="width:35px"></label></h2>
    <div style="width:40px;margin-top:-20px;padding:7px 0px 15px 0px">
        <div id="borderLeft"></div>
    </div>
    <form class="forms_form">
        <fieldset class="forms_fieldset">
            <p id="feedback_message_register"></p>
            <table>
                <tr>
                    <td>
                        <div class="forms_field">
                            <input type="text" placeholder="Name" id="name" name="name" class="forms_field-input"/>
                    </td>
                    <td><label for="login-mail"><i class="fa fa-user-o"></i></label>
</div></td></tr>

<tr>
    <td>
        <div class="forms_field">
            <input type="email" placeholder="Email" id="register_email" name="register_email"
                   class="forms_field-input"/>
    </td>
    <td><label for="login-mail"><i class="fa fa-envelope-o"></i></label></div></td>
</tr>

<tr>
    <td>
        <div class="forms_field">
            <input type="password" placeholder="Password" id="register_password" name="register_password"
                   class="forms_field-input"/>
    </td>
    <td><label for="login-mail"><i class="fa fa-user-o"></i></label></div></td>
</tr>
</table>


</fieldset>
<div class="forms_buttons">
    <input type="button" value="Sign up" class="forms_buttons-action" name="register" id="register_submit">
</div>
</form>
</div>
</div>

