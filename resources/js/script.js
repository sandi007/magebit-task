/**
 * Variables
 */
const signupButton = document.getElementById('signup-button'),
    loginButton = document.getElementById('login-button'),
    userForms = document.getElementById('user_options-forms');

/**
 * Add event listener to the "Sign Up" button
 */
signupButton.addEventListener('click', () => {
    userForms.classList.remove('bounceRight');
    userForms.classList.add('bounceLeft');
    clearField();
}, false);

/**
 * Add event listener to the "Login" button
 */
loginButton.addEventListener('click', () => {
    userForms.classList.remove('bounceLeft');
    userForms.classList.add('bounceRight');
    clearField();
}, false);
//# sourceURL=pen.js

function clearField() {
    document.getElementById('name').value='';
    document.getElementById('register_email').value='';
    document.getElementById('name').value='';
    document.getElementById('register_password').value='';
    document.getElementById('email').value='';
    document.getElementById('password').value='';
    document.getElementById('feedback_message_register').innerText='';
    document.getElementById('feedback_message').innerText='';
}