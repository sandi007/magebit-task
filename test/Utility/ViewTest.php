<?php


namespace Test\Utility;


use App\Utility\Config;
use App\Utility\View;
use Exception;
use PHPUnit\Framework\TestCase;

class ViewTest extends TestCase
{
    public function test_method_view_path_expect_path_string()
    {
        //arrange
        $view_path = '/resources/Views/';

        //act
        $view = new View();

        //assert
        $this->assertContains($view_path, $view->viewPath());
    }

    public function test_method_set_layout_expect_is_layout_true()
    {
        //act
        $view = new View();
        $view->setLayout(true);

        //assert
        $this->assertTrue($view->_layout);
    }

    public function test_method_set_layout_expect_default_header()
    {
        //arrange
        $header_layout =  Config::get('app.VIEW.DEFAULT_LAYOUT_HEADER');

        //act
        $view = new View();
        $view->setLayout(true);

        //assert
        $this->assertContains($header_layout,$view->view_header);
    }


    public function test_method_render_view_without_layout_expect_exception()
    {
        //assert
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("View not found!");

        //act
        $view = new View();
        $view->renderViewWithoutLayout('test');
    }


}