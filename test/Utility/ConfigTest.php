<?php

namespace Test\Utility;

use App\Utility\Config;
use Exception;
use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase
{
    public function test_config_expect_null_for_invalid_argument()
    {
        //act
        $val = Config::get('');

        //assert
        $this->assertNull($val);
    }

    public function test_config_expect_null_for_file_not_found()
    {
        //act
        $val = Config::get('abcd.MYSQL.PDO.DB_USERNAME');

        //assert
        $this->assertNull($val);
    }

    public function test_config_success_with_array()
    {
        //act
        $config = Config::get('database');

        //assert
        $this->assertArrayHasKey("MYSQL", $config);

    }

    public function test_config_success_with_string()
    {
        //arrange
        $actual_value = "root";

        //act
        $username = Config::get('database.MYSQL.PDO.DB_USERNAME');

        //assert
        $this->assertEquals($actual_value, $username);
    }
}