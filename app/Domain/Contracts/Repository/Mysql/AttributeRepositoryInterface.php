<?php


namespace App\Domain\Contracts\Repository\Mysql;


use App\Domain\Contracts\Repository\BaseMysqlRepositoryInterface;
use App\Domain\Models\Attribute;

/**
 * Attribute Database Table query Interface
 * Interface AttributeRepositoryInterface
 * @package App\Domain\Contracts\Repository\Mysql
 */
interface AttributeRepositoryInterface extends BaseMysqlRepositoryInterface
{
    /**
     * Save Attribute in to database table
     * @param Attribute $attribute
     * @return mixed
     */
    public function save(Attribute $attribute);

    /**
     * Update Attribute into database table
     * @param Attribute $attribute
     * @return mixed
     */
    public function update(Attribute $attribute);

    /**
     *  Find Attribute by id from database table
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * Find all Attributes from database table
     * @return mixed
     */
    public function findAll();

    /**
     * Find  Attribute from database by table table name
     * @param $table_name
     * @return mixed
     */
    public function findByTableName($table_name);

    /**
     * Retrieve by column table name and id
     * @param $table_name
     * @param $id
     * @return mixed
     */
    public function getWithValues($table_name, $id);
}