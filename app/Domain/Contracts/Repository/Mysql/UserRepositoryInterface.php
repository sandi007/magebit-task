<?php


namespace App\Domain\Contracts\Repository\Mysql;


use App\Domain\Contracts\Repository\BaseMysqlRepositoryInterface;
use App\Domain\Models\User;

/**
 * User Database Table query Interface
 * Interface UserRepositoryInterface
 * @package App\Domain\Contracts\Repository\Mysql
 */
interface UserRepositoryInterface extends  BaseMysqlRepositoryInterface
{
    /**
     * Save User in to database table
     * @param User $user
     * @return mixed
     */
    public function save(User $user);

    /**
     * Update User into database table
     * @param User $user
     * @return mixed
     */
    public function update(User $user);

    /**
     * Find User by id from database table
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * Find all Users from database table
     * @return mixed
     */
    public function findAll();

    /**
     * Find User by username and password from database table
     * @param $username
     * @param $password
     * @return mixed
     */
    public function findByUsernameAndPassword($username, $password);
}