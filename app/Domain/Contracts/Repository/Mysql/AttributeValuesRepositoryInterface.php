<?php


namespace App\Domain\Contracts\Repository\Mysql;


use App\Domain\Contracts\Repository\BaseMysqlRepositoryInterface;
use App\Domain\Models\AttributeValues;

/**
 * AttributeValues Database Table query Interface
 * Interface AttributeValuesRepositoryInterface
 * @package App\Domain\Contracts\Repository\Mysql
 */
interface AttributeValuesRepositoryInterface extends BaseMysqlRepositoryInterface
{
    /**
     * Save AttributeValues into database table
     * @param AttributeValues $attributeValues
     * @return mixed
     */
   public function save(AttributeValues $attributeValues);

    /**
     * Delete AttributeValues By Attribute id and table Primary key
     * @param AttributeValues $attributeValues
     * @return mixed
     */
   public function deleteByAttributeIdAndTablePrimaryKeyId(AttributeValues $attributeValues);
}