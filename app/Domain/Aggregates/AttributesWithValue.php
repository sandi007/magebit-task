<?php


namespace App\Domain\Aggregates;

/**
 * This is the aggregate model of Attribute entity and AttributeValue Entity
 * Class AttributesWithValue
 * @package App\Domain\Aggregates
 */
class AttributesWithValue
{

    public $id;
    public $name;
    public $label;
    public $type;
    public $status;
    public $value;

    /**
     * AttributesWithValue constructor.
     * @param array $data
     */
    public function __construct($data = array())
    {
        $this->setProperties($data);
    }

    /**
     * @param array $data
     */
    private function setProperties(array $data)
    {
        if (is_array($data) && !empty($data)) {
            $this->id = isset($data['id']) ? $data['id'] : null;
            $this->name = isset($data['name']) ? $data['name'] : null;
            $this->label = isset($data['label']) ? $data['label'] : null;
            $this->type = isset($data['type']) ? $data['type'] : null;
            $this->status = isset($data['status']) ? $data['status'] : 0;
            $this->value = isset($data['value']) ? $data['value'] : null;
        }
    }
}