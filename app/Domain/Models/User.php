<?php


namespace App\Domain\Models;

/**
 * User Domain model with properties
 * Class User
 * @package App\Domain\Models
 */
class User
{
    public $id;
    public $name;
    public $email;
    public $password;

    public static string $table = 'users';

    /**
     * User constructor.
     * @param array $data
     */
    public function __construct($data = array())
    {
        $this->setProperties($data);
    }

    /**
     * @param $data
     * set properties
     */
    private function setProperties($data)
    {
        if (is_array($data) && !empty($data)) {
            $this->id = isset($data['id']) ? $data['id'] : null;
            $this->name = isset($data['name']) ? $data['name'] : null;
            $this->email = isset($data['email']) ? $data['email'] : null;
            $this->password = isset($data['password']) ? $data['password'] : null;
        }
    }
}