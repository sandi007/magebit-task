<?php


namespace App\Domain\Models;

/**
 * AttributeValue Domain model with properties
 * Class AttributeValues
 * @package App\Domain\Models
 */
class AttributeValues
{
    public $id;
    public $attributes_id;
    public $table_primary_key_id;
    public $value;

    public static string $table='attribute_values';

    /**
     * Attributes constructor.
     * @param $data
     */
    public function __construct($data = array())
    {
        $this->setProperties($data);
    }

    private function setProperties(array $data)
    {
        if (is_array($data) && !empty($data)) {
            $this->id = isset($data['id']) ? $data['id'] : null;
            $this->attributes_id = isset($data['attributes_id']) ? $data['attributes_id'] : null;
            $this->table_primary_key_id = isset($data['table_primary_key_id']) ? $data['table_primary_key_id'] : null;
            $this->value = isset($data['value']) ? $data['value'] : null;
        }
    }
}