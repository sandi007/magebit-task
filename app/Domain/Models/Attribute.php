<?php


namespace App\Domain\Models;

/**
 * Attribute domain model with properties
 * Class Attribute
 * @package App\Domain\Models
 */
class Attribute
{
    public $id;
    public $name;
    public $label;
    public $type;
    public $table_name;
    public $status;

    public static string $table='attributes';

    /**
     * Attributes constructor.
     * @param $data
     */
    public function __construct($data = array())
    {
        $this->setProperties($data);
    }

    private function setProperties(array $data)
    {
        if (is_array($data) && !empty($data)) {
            $this->id = isset($data['id']) ? $data['id'] : null;
            $this->name = isset($data['name']) ? $data['name'] : null;
            $this->label = isset($data['label']) ? $data['label'] : null;
            $this->type = isset($data['type']) ? $data['type'] : null;
            $this->table_name = isset($data['table_name']) ? $data['table_name'] : null;
            $this->status = isset($data['status']) ? $data['status'] : 0;
        }
    }
}