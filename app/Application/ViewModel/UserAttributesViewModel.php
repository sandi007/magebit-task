<?php


namespace App\Application\ViewModel;


use App\Domain\Models\Attribute;
use App\Domain\Models\User;

/**
 * View model for user attributes
 *
 * Class UserAttributesViewModel
 * @package App\Application\ViewModel
 */
class UserAttributesViewModel
{
    /**
     * @var User
     */
    public User $user;
    /**
     * @var array
     */
    public array $attributes;

    /**
     * UserAttributesViewModel constructor.
     * @param array $data
     */
    public function __construct($data = array())
    {
        if (!empty($data))
            $this->setProperties($data);
    }

    /**
     * @param $data
     */
    private function setProperties($data)
    {
        if (isset($data["user"]) && $data["user"] instanceof User) {
            $this->user = $data["user"];
        }

        if (isset($data["attributes"]) && !empty($data["attributes"])) {
            foreach ($data["attributes"] as $attribute) {
                if ($attribute instanceof Attribute)
                    $this->attributes[] = $attribute;
            }
        }
    }
}