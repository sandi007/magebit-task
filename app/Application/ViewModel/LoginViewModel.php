<?php


namespace App\Application\ViewModel;

/**
 * View model for login page
 * Class LoginViewModel
 * @package App\Application\ViewModel
 */
class LoginViewModel
{
    /**
     * @var bool
     */
   public bool $status;
    /**
     * @var string
     */
   public string $message;

    /**
     * LoginViewModel constructor.
     * @param $status
     * @param $message
     */
   public function __construct($status,$message)
   {
       $this->status =$status;
       $this->message=$message;
   }
}