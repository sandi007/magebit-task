<?php


namespace App\Application\ViewModel;


use App\Domain\Models\User;
use App\Domain\Aggregates\AttributesWithValue;

/**
 *
 * Class UserAttributesWithValueViewModel
 * @package App\Application\ViewModel
 */
class UserAttributesWithValueViewModel
{
    public User $user;
    public array $attributes;

    public function __construct($data = array())
    {
        if (!empty($data))
            $this->setProperties($data);
    }

    private function setProperties($data)
    {
        if (isset($data["user"]) && $data["user"] instanceof User) {
            $this->user = $data["user"];
        }

        if (isset($data["attributes_with_values"]) && !empty($data["attributes_with_values"])) {
            foreach ($data["attributes_with_values"] as $attribute) {
                if ($attribute instanceof AttributesWithValue)
                    $this->attributes[] = $attribute;
            }
        }
    }
}