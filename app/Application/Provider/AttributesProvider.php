<?php


namespace App\Application\Provider;


use App\Application\Contracts\Provider\AttributesProviderInterface;
use App\Domain\Contracts\Repository\Mysql\AttributeRepositoryInterface;

class AttributesProvider implements AttributesProviderInterface
{
    /**
     * @var AttributeRepositoryInterface
     */
    private AttributeRepositoryInterface $attributeRepository;


    /**
     * Initialize $attributeRepository
     * AttributesProvider constructor.
     * @param AttributeRepositoryInterface $attributeRepository
     */
    public function __construct(AttributeRepositoryInterface $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * Implementation of retrieve attributes entity data with attribute_values entity function
     * filter by table name and primary id
     * @param $table_name
     * @param $primary_id
     * @return mixed
     */
    public function getAttributesWithValue($table_name, $primary_id)
    {
        return $this->attributeRepository->getWithValues($table_name, $primary_id);
    }
}