<?php


namespace App\Application\Provider;


use App\Application\Contracts\Provider\AttributeValueProviderInterface;
use App\Domain\Contracts\Repository\Mysql\AttributeValuesRepositoryInterface;
use App\Domain\Models\AttributeValues;

class AttributeValueProvider implements AttributeValueProviderInterface
{
    /**
     * @var AttributeValuesRepositoryInterface
     */
    private AttributeValuesRepositoryInterface $attributeValueRepository;

    /**
     * Initialize class with $attributeValueRepository
     * AttributeValueProvider constructor.
     * @param AttributeValuesRepositoryInterface $attributeValueRepository
     */
    public function __construct(AttributeValuesRepositoryInterface $attributeValueRepository)
    {
        $this->attributeValueRepository = $attributeValueRepository;
    }

    /**
     * Insert data into attribute table
     * @param AttributeValues $attributeValues
     * @return mixed|void
     */
    public function insert(AttributeValues $attributeValues)
    {
        $this->attributeValueRepository->deleteByAttributeIdAndTablePrimaryKeyId($attributeValues);
        $this->attributeValueRepository->save($attributeValues);
    }
}