<?php


namespace App\Application\Contracts\Service;

/**
 * Execute application logic
 * and provide response to LoginController
 *
 * Interface LoginRequestServiceInterface
 * @package App\Application\Contracts\Service
 */
interface LoginRequestServiceInterface
{
    /**
     * Definition for login request
     * @param $user_name
     * @param $password
     * @return mixed
     */
    public function login_request($user_name, $password);

    /**
     * Definition logout request
     * @return mixed
     */
    public function logout_request();
}