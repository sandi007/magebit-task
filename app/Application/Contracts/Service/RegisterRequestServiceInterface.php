<?php


namespace App\Application\Contracts\Service;

/**
 * Execute application logic
 * and provide response to RegistrationController
 *
 * Interface RegisterRequestServiceInterface
 * @package App\Application\Contracts\Service
 */
interface RegisterRequestServiceInterface
{
    /**
     * Declare register user application method
     *
     * @param array $registration_data
     * @return mixed
     */
    public function register_user(array $registration_data);
}