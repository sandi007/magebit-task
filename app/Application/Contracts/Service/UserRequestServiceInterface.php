<?php


namespace App\Application\Contracts\Service;

/**
 * Execute application logic
 * and provide response to UserController
 *
 * Interface UserRequestServiceInterface
 * @package App\Application\Contracts\Service
 */
interface UserRequestServiceInterface
{
    /**
     * Retrieve all user from database
     * @return mixed
     */
    public function getAllUsers();

    /**
     * Retrieve user with its attributes
     * @param $id
     * @return mixed
     */
    public function getUserWithAttributesValues($id);

    /**
     * Update user with attribute values
     * @param array $data
     * @return mixed
     */
    public function updateUserWithAttributesValues(array $data);
}