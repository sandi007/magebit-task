<?php


namespace App\Application\Contracts\Provider;

use App\Domain\Models\AttributeValues;

/**
 * Provide AttributesValue related entity application logic
 *
 * Interface AttributeValueProviderInterface
 * @package App\Application\Contracts\Provider
 */
interface AttributeValueProviderInterface
{
    /**
     * Defination of insert data attribute_values entity
     * @param AttributeValues $attributeValues
     * @return mixed
     */
    public function insert(AttributeValues $attributeValues);
}