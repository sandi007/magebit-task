<?php


namespace App\Application\Contracts\Provider;

/**
 * Provide Attributes entity related application logic
 *
 * Interface AttributesProviderInterface
 * @package App\Application\Contracts\Provider
 */
interface AttributesProviderInterface
{
    /**
     * Definition of retrieve attributes entity data with attribute_values entity
     * filter by table name and primary id
     * @param $table_name
     * @param $primary_id
     * @return mixed
     */
   public function getAttributesWithValue($table_name,$primary_id);
}