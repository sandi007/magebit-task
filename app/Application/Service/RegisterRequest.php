<?php


namespace App\Application\Service;

use App\Application\Contracts\Service\RegisterRequestServiceInterface;
use App\Domain\Contracts\Repository\Mysql\UserRepositoryInterface;
use App\Domain\Models\User;
use App\Utility\Flash;
use App\Utility\Hash;


class RegisterRequest implements RegisterRequestServiceInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * Initialize class with $userRepository
     * RegisterRequest constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Register user data insert into database successfully
     * @param array $registration_data
     */
    public function register_user(array $registration_data)
    {
        try {
            if (isset($registration_data["password"])) {
                $registration_data["password"] = Hash::generate($registration_data["password"]);
            }
            $user = new User($registration_data);
            $this->userRepository->save($user);
        } catch (\Exception $e) {
            Flash::fail("Error " . $e->getMessage());
        }
    }
}