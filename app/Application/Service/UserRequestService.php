<?php


namespace App\Application\Service;

use App\Application\Contracts\Provider\AttributesProviderInterface;
use App\Application\Contracts\Provider\AttributeValueProviderInterface;
use App\Application\Contracts\Service\UserRequestServiceInterface;
use App\Application\ViewModel\UserAttributesWithValueViewModel;
use App\Domain\Contracts\Repository\Mysql\UserRepositoryInterface;
use App\Domain\Models\AttributeValues;
use App\Domain\Models\User;
use App\Utility\Flash;
use App\Utility\Hash;

class UserRequestService implements UserRequestServiceInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;
    /**
     * @var AttributesProviderInterface
     */
    private AttributesProviderInterface $attributesProvider;
    /**
     * @var AttributeValueProviderInterface
     */
    private AttributeValueProviderInterface $attributeValueProvider;

    /**
     * UserRequestService constructor.
     * @param UserRepositoryInterface $userRepository
     * @param AttributesProviderInterface $attributesProvider
     * @param AttributeValueProviderInterface $attributeValueProvider
     */
    public function __construct(UserRepositoryInterface $userRepository,
                                AttributesProviderInterface $attributesProvider,
                                AttributeValueProviderInterface $attributeValueProvider)
    {
        $this->userRepository = $userRepository;
        $this->attributesProvider = $attributesProvider;
        $this->attributeValueProvider = $attributeValueProvider;
    }

    /**
     * Implementation of retrieving all user from database
     * @return mixed
     */
    public function getAllUsers()
    {
        return $this->userRepository->findAll();
    }

    /**
     * Implementation of retrieving user with its attributes and values
     * @param $id
     * @return UserAttributesWithValueViewModel|mixed
     */
    public function getUserWithAttributesValues($id)
    {
        $attributes_with_values = $this->attributesProvider->getAttributesWithValue(User::$table, $id);
        $user = $this->userRepository->find($id);

        return new UserAttributesWithValueViewModel(['user' => $user, 'attributes_with_values' => $attributes_with_values]);
    }

    /**
     *  Implementation updating user with attribute values
     * @param array $data
     * @return mixed|void
     */
    public function updateUserWithAttributesValues($data)
    {
        try {
            if (isset($data["password"]) && !empty($data["password"])) {
                $data["password"] = Hash::generate($data["password"]);
            }
            $user = new User($data);
            $this->userRepository->update($user);

            if (!empty($data['attributes'])) {
                foreach ($data['attributes'] as $attribute) {

                    $this->attributeValueProvider->insert(
                        new AttributeValues(['attributes_id' => key($attribute),
                            'table_primary_key_id' => $user->id,
                            'value' => reset($attribute)])
                    );
                }
            }
        } catch (\Exception $e) {
            Flash::fail("Some error! Please tru=y again");
        }
    }
}