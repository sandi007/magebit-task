<?php


namespace App\Application\Service;


use App\Application\Contracts\Service\LoginRequestServiceInterface;
use App\Application\ViewModel\LoginViewModel;
use App\Domain\Contracts\Repository\Mysql\UserRepositoryInterface;
use App\Utility\Config;
use App\Utility\Hash;
use App\Utility\Session;

class LoginRequest implements LoginRequestServiceInterface
{
    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * Initialize class with $userRepository
     * LoginRequest constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Implement Logic Login Request
     * It will retrieve data from user repository an check username and password exist or not
     * @param $user_name
     * @param $password
     * @return LoginViewModel|mixed
     */
    public function login_request($user_name, $password)
    {
        try {
            $password = Hash::generate($password);
            if ($user = $this->userRepository->findByUsernameAndPassword($user_name, $password)) {
                Session::put(Config::get('app.SESSION_USER'), $user->id);
                return new LoginViewModel(true, 'Login Success !');
            }
            return new LoginViewModel(false, 'Invalid Login Credentials !');
        } catch (\Exception $e) {
            return new LoginViewModel(false, 'Some Error Please try again later !');
        }
    }

    /**
     * Destroy session
     * @return mixed|void
     */
    public function logout_request()
    {
        Session::delete(Config::get('app.SESSION_USER'));
    }
}