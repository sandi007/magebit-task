<?php


namespace App\Infrastructure\Repository\Mysql;


use App\Domain\Contracts\Repository\Mysql\AttributeRepositoryInterface;
use App\Domain\Models\Attribute;
use App\Domain\Aggregates\AttributesWithValue;
use App\Infrastructure\Repository\BaseMysqlRepository;
use PDO;

class AttributeRepository extends BaseMysqlRepository implements AttributeRepositoryInterface
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Implement Insert Attribute query
     * @param Attribute $attribute
     * @return mixed
     */
    public function save(Attribute $attribute)
    {
        if (isset($attribute->id)) {
            return $this->update($attribute);
        }
        $pdo_statement = $this->connection->prepare("
                                                INSERT INTO `attributes`(`name`,`label`,`type`, `table_name`, `status`)
                                                 VALUES (:attribute_name,:label,:attribute_type,:attribute_table_name,:status)");

        $pdo_statement->bindParam(':attribute_name', $attribute->name);
        $pdo_statement->bindParam(':label', $attribute->label);
        $pdo_statement->bindParam(':attribute_type', $attribute->type);
        $pdo_statement->bindParam(':attribute_table_name', $attribute->table_name);
        $pdo_statement->bindParam(':status', $attribute->status);

        return $pdo_statement->execute();
    }

    /**
     * Implement Update Attribute query
     * @param Attribute $attribute
     * @return mixed
     */
    public function update(Attribute $attribute)
    {
        if (!isset($attribute->id)) {
            throw new \LogicException('Can not update attribute that does not exist in database');
        }
        $pdo_statement = $this->connection->prepare("UPDATE `attributes` 
                                                     SET `name`=:attribute_name,`label`=:label,`type`=:attribute_type,
                                                    `table_name`=:attribute_table_name,`status`=:status 
                                                     WHERE `id`=:id");

        $pdo_statement->bindParam(':attribute_name', $attribute->name);
        $pdo_statement->bindParam(':label', $attribute->label);
        $pdo_statement->bindParam(':attribute_type', $attribute->type);
        $pdo_statement->bindParam(':attribute_table_name', $attribute->table_name);
        $pdo_statement->bindParam(':status', $attribute->status);
        $pdo_statement->bindParam(':id', $attribute->id);

        return $pdo_statement->execute();
    }

    /**
     * Implement Find by id query
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        $pdo_statement = $this->connection->prepare('SELECT * FROM `attributes`
                                                               where `id` = :id');
        $pdo_statement->bindParam(':id', $id);
        $pdo_statement->execute();
        $pdo_statement->setFetchMode(PDO::FETCH_INTO, new Attribute());

        return $pdo_statement->fetch();
    }

    /**
     * Implement Select all value query
     * @return mixed
     */
    public function findAll()
    {
        $pdo_statement = $this->connection->prepare('SELECT * from `attributes`');
        $pdo_statement->execute();
        return $pdo_statement->fetchAll(PDO::FETCH_CLASS, Attribute::class);
    }

    /**
     * Implement Find by table name query
     * @param $table_name
     * @return mixed
     */
    public function findByTableName($table_name)
    {
        $pdo_statement = $this->connection->prepare('SELECT * FROM `attributes`
                                                               where `table_name` = :attribute_table_name');
        $pdo_statement->bindParam(':attribute_table_name', $table_name);
        $pdo_statement->execute();

        return $pdo_statement->fetchAll(PDO::FETCH_CLASS, Attribute::class);
    }


    /**
     * Implement Select with attribute value query
     * @param $table_name
     * @param $id
     * @return mixed
     */
    public function getWithValues($table_name, $id)
    {
        $pdo_statement = $this->connection->prepare('SELECT `attributes`.`id`,`attributes`.`name`,`attributes`.`label`,`attributes`.`type`,`attributes`.`table_name`, 
                                                     `attributes`.`status`,`attribute_value_sub`.`value` FROM `attributes` LEFT JOIN 
                                                      (SELECT `attribute_values`.`attributes_id` as `attribute_id`,`attribute_values`.`value` 
                                                      as `value` from `attribute_values`WHERE `attribute_values`.`table_primary_key_id`=:primary_key_id) 
                                                      as `attribute_value_sub` on `attributes`.`id` = `attribute_value_sub`.`attribute_id` where 
                                                      `attributes`.`table_name`=:attribute_table_name && `attributes`.`status`=1');

        $pdo_statement->bindParam(':attribute_table_name', $table_name);
        $pdo_statement->bindParam(':primary_key_id', $id);
        $pdo_statement->execute();

        return $pdo_statement->fetchAll(PDO::FETCH_CLASS, AttributesWithValue::class);
    }
}