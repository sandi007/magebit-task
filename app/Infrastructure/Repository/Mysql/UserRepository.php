<?php


namespace App\Infrastructure\Repository\Mysql;


use App\Domain\Contracts\Repository\Mysql\UserRepositoryInterface;
use App\Domain\Models\User;
use App\Infrastructure\Repository\BaseMysqlRepository;
use PDO;

class UserRepository extends BaseMysqlRepository implements UserRepositoryInterface
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Implement Insert User query
     * @param User $user
     * @return bool|mixed|void
     */
    public function save(User $user)
    {
        if (isset($user->id)) {
            return $this->update($user);
        }
        $pdo_statement = $this->connection->prepare("
                                                INSERT INTO `users`
                                                (`name`,`email`,`password`)
                                                VALUES(:full_name,:email,:password)");

        $pdo_statement->bindParam(':full_name', $user->name);
        $pdo_statement->bindParam(':email', $user->email);
        $pdo_statement->bindParam(':password', $user->password);

        return $pdo_statement->execute();
    }

    /**
     * Implement Update User query
     * @param User $user
     * @return mixed|void
     */
    public function update(User $user)
    {
        if (!isset($user->id)) {
            throw new \LogicException('Can not update user that does not exist in database');
        }

        if (isset($user->password) && $user->password != null) {
            $pdo_statement = $this->connection->prepare("UPDATE `users` SET 
                             `name`=:full_name,`email`=:email,`password`=:password WHERE `id`= :id");
            $pdo_statement->bindParam(':password', $user->password);
        } else {
            $pdo_statement = $this->connection->prepare("UPDATE `users` SET
                            `name`=:full_name,`email`=:email WHERE `id`= :id");
        }

        $pdo_statement->bindParam(':full_name', $user->name);
        $pdo_statement->bindParam(':email', $user->email);
        $pdo_statement->bindParam(':id', $user->id);

        return $pdo_statement->execute();
    }

    /**
     * Implement Select By Id query
     * @param $id
     * @return mixed|void
     */
    public function find($id)
    {
        $pdo_statement = $this->connection->prepare('SELECT `id`,`name`,`email` FROM `users`
                                                               where `id` = :id');
        $pdo_statement->bindParam(':id', $id);
        $pdo_statement->execute();
        $pdo_statement->setFetchMode(PDO::FETCH_INTO, new User());

        return $pdo_statement->fetch();
    }

    /**
     * Implement select all query
     * @return mixed|void
     */
    public function findAll()
    {
        $pdo_statement = $this->connection->prepare('SELECT `id`,`name`,`email` from `users`');
        $pdo_statement->execute();

        return $pdo_statement->fetchAll(PDO::FETCH_CLASS, User::class);
    }

    /**
     * Implement select user by username and password query
     * @param $username
     * @param $password
     * @return mixed
     */
    public function findByUsernameAndPassword($username, $password)
    {
        $pdo_statement = $this->connection->prepare('SELECT `id`,`name`,`email` FROM `users`
                                                               where `email` = :username and `password` =:password');
        $pdo_statement->bindParam(':username', $username);
        $pdo_statement->bindParam(':password', $password);
        $pdo_statement->setFetchMode(PDO::FETCH_INTO, new User());
        $pdo_statement->execute();

        return $pdo_statement->fetch();
    }
}