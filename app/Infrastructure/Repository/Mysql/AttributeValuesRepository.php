<?php


namespace App\Infrastructure\Repository\Mysql;


use App\Domain\Contracts\Repository\Mysql\AttributeValuesRepositoryInterface;
use App\Domain\Models\AttributeValues;
use App\Infrastructure\Repository\BaseMysqlRepository;

class AttributeValuesRepository extends BaseMysqlRepository implements AttributeValuesRepositoryInterface
{
    /**
     * AttributeValuesRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *  Implement AttributeValues insert query
     * @param AttributeValues $attributeValues
     * @return mixed
     */
    public function save(AttributeValues $attributeValues)
    {
        $pdo_statement = $this->connection->prepare("
                                                     INSERT INTO `attribute_values`(`attributes_id`, `table_primary_key_id`, `value`) 
                                                      VALUES (:attribute_id,:table_primary_key_id,:val)");

        $pdo_statement->bindParam(':attribute_id', $attributeValues->attributes_id);
        $pdo_statement->bindParam(':table_primary_key_id', $attributeValues->table_primary_key_id);
        $pdo_statement->bindParam(':val', $attributeValues->value);

        return $pdo_statement->execute();
    }

    /**
     * Implement AttributeValues delete query by attribute id and table primary key id fields
     * @param AttributeValues $attributeValues
     * @return mixed|void
     */
    public function deleteByAttributeIdAndTablePrimaryKeyId(AttributeValues $attributeValues)
    {
        $pdo_statement = $this->connection->prepare(" DELETE FROM `attribute_values` WHERE `attributes_id` =:attributes_id 
                                                      and `table_primary_key_id` = :table_primary_key_id");

        $pdo_statement->bindParam(':attributes_id', $attributeValues->attributes_id);
        $pdo_statement->bindParam(':table_primary_key_id', $attributeValues->table_primary_key_id);

        $pdo_statement->execute();
    }
}