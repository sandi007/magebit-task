<?php


namespace App\Infrastructure\Repository;


use App\Domain\Contracts\Repository\BaseMysqlRepositoryInterface;
use App\Utility\Config;
use PDO;

class BaseMysqlRepository implements BaseMysqlRepositoryInterface
{
    protected $connection;

    /**
     * BaseMysqlRepository constructor.
     */
    public function __construct()
    {
        $this->setConnection();
    }

    /**
     * database connection set
     */
    protected function setConnection()
    {
        if ($this->connection === null) {
            $this->connection = new PDO(Config::get('database.MYSQL.PDO.DB_CONNECTION_STRING'),
                Config::get('database.MYSQL.PDO.DB_USERNAME'),
                Config::get('database.MYSQL.PDO.DB_PASSWORD')
            );
            $this->connection->setAttribute(
                PDO::ATTR_ERRMODE,
                PDO::ERRMODE_EXCEPTION
            );
        }
    }
}