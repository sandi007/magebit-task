<?php


namespace App\Utility;


class Session
{
    /**
     * Initialize session
     */
    public static function init()
    {
        if (session_id() == "")
            session_start();
    }

    /**
     * Set session by key and value
     * @param string $key
     * @param mixed $value
     * @return string
     */
    public static function put($key, $value)
    {
        return ($_SESSION[$key] = $value);
    }

    /**
     * Retrieve session value
     * @param string $key
     * @return string| nothing
     */
    public static function get($key)
    {
        if (self::exists($key)) {
            return ($_SESSION[$key]);
        }
    }

    /**
     * Check if a session exist
     * @param string $key
     * @return bool
     */
    public static function exists($key)
    {
        return (isset($_SESSION[$key]));
    }

    /**
     * Destroy all sessions
     */
    public static function destroy()
    {
        session_destroy();
    }

    /**
     * Delete a session by session key
     * @param string $key
     * @return bool
     */
    public static function delete($key)
    {
        if (Self::exists($key)) {
            unset($_SESSION[$key]);
            return true;
        }
        return false;
    }
}