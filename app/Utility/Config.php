<?php


namespace App\Utility;

use Exception;

/**
 * This Class class will access files form config folder
 * and retrieve data
 * Class Config
 * @package App\Utility
 */
class Config
{
    /**
     * @var string
     */
    private static string  $config_folder = '../../../config';

    /**
     * Retrieve config value from key
     * @param $key
     * @return mixed|null
     */
    public static function get($key)
    {
        try {
            $path = self::configPath();

            self::checkArgumentNullOrEmpty($key);

            $explode_config_key = explode('.', $key);

            $config_file = $path . self::$config_folder . '/' . $explode_config_key[0] . '.php';

            self::checkConfigFileExist($config_file);

            $config_item = require $config_file;

            if (count($explode_config_key) == 1)
                return $config_item;

            array_shift($explode_config_key);

            return self::findConfigValue($config_item, $explode_config_key);
        }
        catch (Exception $e)
        {
            return  null;
        }
    }

    /**
     * Return config path
     * @return string|string[]
     */
    private static function configPath()
    {
        return str_replace('\\', '/', __DIR__);
    }

    /**
     * Argument validation check
     * @param $key
     * @throws Exception
     */
    private static function checkArgumentNullOrEmpty($key)
    {
        if ($key == null || $key == '')
            throw new Exception("No Argument pass to the Config");
    }


    /**
     * Check if config file exist else throw exception
     * @param $config_file
     * @throws Exception
     */
    public static function checkConfigFileExist($config_file)
    {
        if (!file_exists($config_file))
            throw new Exception("Config File not found");
    }

    /**
     * Find config value
     * @param $config_item
     * @param $config_key
     * @return mixed|null
     */
    private static function findConfigValue($config_item, $config_key)
    {
        $key = $config_key[0];

        if (!isset($config_item[$key]))
            return null;

        if (isset($config_item[$key]) && count($config_key) == 1)
            return $config_item[$key];

        array_shift($config_key);

        return self::findConfigValue($config_item[$key], $config_key);
    }
}