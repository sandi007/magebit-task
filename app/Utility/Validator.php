<?php


namespace App\Utility;

/**
 * This class will validate all form validation input
 * Class Validator
 * @package App\Utility
 */

class Validator
{
    /**
     * validation status true/false
     * @var bool
     */
    public bool $status;

    /**
     * validation message
     * @var string
     */
    public string $message;

    /**
     * Validator constructor.
     * @param $status
     * @param $message
     */
    public function __construct($status, $message)
    {
        $this->status = $status;
        $this->message = $message;
    }

    /**
     * validation logic,
     * Check by $rules
     * @param $input_item
     * @param array $rules
     * @return Validator
     */
    public static function validate($input_item, $rules = [])
    {
        $validation = self::checkRuleKeyExist($input_item, $rules);

        if (!$validation->status) {
            return $validation;
        }

        foreach ($input_item as $item => $item_value) {
            if (key_exists($item, $rules)) {

                foreach ($rules[$item] as $rule => $rule_value) {
                    if (is_int($rule))
                        $rule = $rule_value;

                    switch ($rule) {
                        case 'required':
                            if (empty($item_value) && $rule_value) {
                                $validation = new Validator(false, $item . " field required!");
                            }
                            break;
                        case 'email':
                            if (!filter_var($item_value, FILTER_VALIDATE_EMAIL)) {
                                $validation = new Validator(false, "Please enter valid email for " . $item . ' field!');
                            }
                            break;
                    }
                }
            }
        }
        return $validation;
    }

    /**
     * check if input key field and rule key match
     * if not match return input missing
     * @param $input_item
     * @param array $rules
     * @return Validator
     */
    public static function checkRuleKeyExist($input_item, $rules = [])
    {
        if (!empty($rules)) {
            foreach ($rules as $item => $item_value) {
                if (!key_exists($item, $input_item)) {
                    return new Validator(false, "Validation failed! " . $item . " input missing!");
                }
            }
        }
        return new Validator(true, '');
    }
}