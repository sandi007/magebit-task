<?php

namespace App\Utility;

/**
 * This class will redirect to specific web url path
 * Class Redirect
 * @package App\Utility
 */
class Redirect
{
    /**
     * Redirect to url path
     * @param $path
     * @throws \Exception
     */
    public static function to($path)
    {
        header("location: " . Config::get('app.BASE_URL') . $path);
        exit();
    }
}