<?php


namespace App\Utility;

use App\Domain\Models\Attribute;
use App\Domain\Aggregates\AttributesWithValue;
use Exception;
use Psr\Http\Message\ResponseInterface;

class View
{
    /**
     * @var bool
     */
    public bool $_layout = false;
    /**
     * @var string
     */
    public string $view_header;
    /**
     * @var string
     */
    public string $view_footer;

    /**
     * Return view path
     * @return string
     * @throws Exception
     */
    public function viewPath()
    {
        return str_replace('\\', '/', __DIR__) . '../../../' . Config::get('app.VIEW.PATH');
    }

    /**
     * @param $is_layout
     * @param null $view_header
     * @param null $view_footer
     * @throws Exception
     */
    public function setLayout($is_layout, $view_header = null, $view_footer = null)
    {
        $this->_layout = $is_layout;

        $this->view_header = ($view_header == null)
            ? self::viewPath() . Config::get('app.VIEW.DEFAULT_LAYOUT_HEADER')
            : self::viewPath() . $view_header;


        $this->view_footer = ($view_footer == null)
            ? self::viewPath() . Config::get('app.VIEW.DEFAULT_LAYOUT_FOOTER')
            : self::viewPath() . $view_footer;
    }

    /**
     * @param $filename
     * @throws Exception
     */
    public function renderViewWithoutLayout($filename)
    {
        $view = self::viewPath() . str_replace('.', '/', $filename) . '.php';

        if (!file_exists($view))
            throw new Exception("View not found!");

        require $view;
    }

    /**
     * @param $view_name
     * @throws Exception
     */
    public function renderViewWithLayout($view_name)
    {

        $view = self::viewPath() . str_replace('.', '/', $view_name) . '.php';


        if (!file_exists($this->view_header))
            throw new Exception("Layout header not found!");

        if (!file_exists($this->view_footer))
            throw new Exception("Layout footer not found!");

        if (!file_exists($view))
            throw new Exception("View not found!");

        require $this->view_header;
        require $view;
        require $this->view_footer;
    }

    /**
     * Render view page
     * @param ResponseInterface $response
     * @param $view_name
     * @param null $data
     * @return ResponseInterface
     * @throws Exception
     */
    public function render(ResponseInterface $response, $view_name, $data = null)
    {
        if ($data) {
            foreach ($data as $key => $value) {
                $this->{$key} = $value;
            }
        }

        if (!$this->_layout) {
            self::renderViewWithoutLayout($view_name);
            return $response;
        }

        self::renderViewWithLayout($view_name);

        return $response;
    }

    /**
     * @throws Exception
     */
    public function renderFeedbackMessage()
    {
        echo '<span style="color:green">' . Flash::session(Config::get('app.SESSION_FLASH_SUCCESS_KEY')) . '</span>';
        echo '<span style="color:red">' . Flash::session(Config::get('app.SESSION_FLASH_FAIL_KEY')) . '</span>';
    }

    /**
     * @return mixed|null
     * @throws Exception
     */
    public function webroot()
    {
        return Config::get('app.BASE_URL');
    }

    /**
     * @param $attributes
     */
    public function generateAttributeFormElements($attributes)
    {
        if (!empty($attributes)) {
            foreach ($attributes as $attribute) {
                if ($attribute instanceof AttributesWithValue || $attribute instanceof Attribute) {
                    $this->generateFormElement($attribute);
                }
            }
        }
    }

    /**
     * Generate form elemant
     * @param $attributes
     */
    private function generateFormElement($attributes)
    {
        switch ($attributes->type) {
            case "text":
                echo $this->generateTextBox($attributes);
                break;
            case "textarea":
                echo $this->generateTextArea($attributes);
                break;
        }
    }

    /**
     * Generate text box for attribute
     * @param $attributes
     * @return string
     */
    private function generateTextBox($attributes)
    {
        $value = (isset($attributes->value)) ? $attributes->value : '';
        return '<label for="' . $attributes->name . '"><b>' . $attributes->label . '</b></label><input type="text"  name="attributes[' . $attributes->name . '][' . $attributes->id . ']" value="' . $value . '"></br>';
    }

    /**
     * Generate text area for attribute
     * @param $attributes
     * @return string
     */
    private function generateTextArea($attributes)
    {
        $value = (isset($attributes->value)) ? $attributes->value : '';
        return '<label for="' . $attributes->name . '"><b>' . $attributes->label . '</b></label><br><textarea name="attributes[' . $attributes->name . '][' . $attributes->id . ']" rows="2" cols="70">' . $value . '</textarea></br>';
    }

}