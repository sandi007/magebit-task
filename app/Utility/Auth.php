<?php
namespace App\Utility;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Auth
{
    /**
     * Check if user is authenticated. If not then redirected to login page
     * @param ResponseInterface $response
     * @return
     * @throws \Exception
     */
    public static function check_authenticated()
    {
        Session::init();
        if (!Session::exists(Config::get('app.SESSION_USER'))) {
            Session::destroy();
           Redirect::to('/login');
        }
    }

    /**
     * Check if user is unauthenticated if not then redirect to index page
     * @param ResponseInterface $response
     * @throws \Exception
     */
    public static function check_unauthenticated()
    {
        Session::init();
        if (Session::exists(Config::get('app.SESSION_USER'))) {
            Redirect::to('/index');
        }
    }
}