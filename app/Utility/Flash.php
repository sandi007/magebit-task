<?php


namespace App\Utility;

/**
 * This utility class for flash session message
 * Class Flash
 * @package App\Utility
 */

class Flash
{
    /**
     * Return session message
     * @param $key
     * @param string $value
     * @return string|null
     */
    public static function session($key, $value = "")
    {
        if (Session::exists($key)) {
            $session = Session::get($key);
            Session::delete($key);
            return $session;
        } elseif (!empty($value)) {
            return (Session::put($key, $value));
        }
        return null;
    }

    /**
     * Return success message from session variable
     * @param string $value
     * @return string|null
     */
    public static function success($value = "")
    {
        try {
            return (self::session(Config::get('app.SESSION_FLASH_SUCCESS_KEY'), $value));
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Return success message from session variable
     * @param string $value
     * @return string|null
     */
    public static function fail($value = "")
    {
        try {
            return (self::session(Config::get('app.SESSION_FLASH_FAIL_KEY'), $value));
        } catch (\Exception $e) {
            return null;
        }
    }
}