<?php


namespace App\Utility;

/**
 * This class will generate hash for password
 * Class Hash
 * @package App\Utility
 */

class Hash
{
    /**
     * Generate sha256 hash code
     * @param $string
     * @return string
     * @throws \Exception
     */
   public static function generate($string)
   {
       return(hash("sha256",$string.Config::get('app.SALT')));
   }
}