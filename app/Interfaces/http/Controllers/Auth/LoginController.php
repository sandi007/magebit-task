<?php

namespace App\Interfaces\http\Controllers\Auth;

use App\Application\Contracts\Service\LoginRequestServiceInterface;
use App\Interfaces\http\Controllers\Controller;
use App\Utility\Auth;
use App\Utility\Config;
use App\Utility\Redirect;
use App\Utility\Validator;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;


class LoginController extends Controller
{
    /**
     * @var LoginRequestServiceInterface
     */
    private LoginRequestServiceInterface $loginRequestService;

    /**
     * Call parent class and set layout and authentication for this controller and initialize $loginRequestService
     * LoginController constructor.
     * @param LoginRequestServiceInterface $login_service
     */
    public function __construct(LoginRequestServiceInterface $login_service)
    {
        parent::__construct();
        $this->loginRequestService = $login_service;
        $this->view->setLayout(true,Config::get('app.VIEW.LOGIN_LAYOUT_HEADER'),Config::get('app.VIEW.LOGIN_LAYOUT_FOOTER'));
    }

    /**
     * Display login page also check user is unauthenticated or not
     * If user unauthenticated then it will redirect to index page
     * GET METHOD
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws \Exception
     */
    public function login(ServerRequestInterface $request, ResponseInterface $response)
    {
        Auth::check_unauthenticated();
        return $this->view->render($response, 'Auth.login');
    }

    /**
     * Login check method. If user exist with proper credentials,it will redirect to index page
     * If user not exist or wrong credentials, it will show error message
     * POST METHOD
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function login_post(ServerRequestInterface $request, ResponseInterface $response)
    {
        $input_data = $request->getParsedBody();
        $rules = [
            'email' => ['required', 'email'],
            'password' => ['required']
        ];
        $validation = Validator::validate($input_data, $rules);

        if (!$validation->status) {
            $response->getBody()->write(json_encode(['success' => false, 'message' => $validation->message]));
            return $response->withHeader('Content-Type', 'application/json');
        }
        $login_check_result = $this->loginRequestService->login_request($input_data["email"], $input_data["password"]);

        if (!$login_check_result->status) {
            $response->getBody()->write(json_encode(['success' => false, 'message' => $login_check_result->message]));
            return $response->withHeader('Content-Type', 'application/json');
        }
        $response->getBody()->write(json_encode(['success' => true, 'message' => $login_check_result->message]));
        return $response->withHeader('Content-Type', 'application/json');
    }

    /**
     * Destroy session and logout user
     * POST METHOD
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     */
    public function logout(ServerRequestInterface $request, ResponseInterface $response)
    {
        $this->loginRequestService->logout_request();
        Redirect::to('/login');
    }
}