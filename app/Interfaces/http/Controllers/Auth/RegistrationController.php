<?php


namespace App\Interfaces\http\Controllers\Auth;


use App\Application\Contracts\Service\RegisterRequestServiceInterface;
use App\Interfaces\http\Controllers\Controller;
use App\Utility\Flash;
use App\Utility\Redirect;
use App\Utility\Validator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class RegistrationController extends Controller
{
    /**
     * @var RegisterRequestServiceInterface
     */
    private RegisterRequestServiceInterface $registerServiceRequest;

    /**
     * Call parent class and initialize $registerServiceRequest
     * RegistrationController constructor.
     * @param RegisterRequestServiceInterface $registerServiceRequest
     */
    public function __construct(RegisterRequestServiceInterface $registerServiceRequest)
    {
        $this->registerServiceRequest = $registerServiceRequest;
        parent::__construct();
    }

    /**
     * Registration request with name,email and password
     * POST METHOD
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function register_post(ServerRequestInterface $request, ResponseInterface $response)
    {
        $input_data = $request->getParsedBody();
        $rules = [
            'name' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required']
        ];
        $validation = Validator::validate($input_data, $rules);

        if (!$validation->status) {
            $response->getBody()->write(json_encode(['success' => false, 'message' => $validation->message]));
            return $response->withHeader('Content-Type', 'application/json');
        }
         $this->registerServiceRequest->register_user($input_data);
        $response->getBody()->write(json_encode(['success' => true, 'message' => 'Registration Successful! Please try login.']));
        return $response->withHeader('Content-Type', 'application/json');
    }
}