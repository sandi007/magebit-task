<?php


namespace App\Interfaces\http\Controllers;


use App\Domain\Contracts\Repository\Mysql\AttributeRepositoryInterface;
use App\Domain\Models\Attribute;
use App\Interfaces\http\Controllers\Controller;
use App\Utility\Auth;
use App\Utility\Config;
use App\Utility\Flash;
use App\Utility\Redirect;
use App\Utility\Validator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class AttributeController extends Controller
{
    /**
     * @var AttributeRepositoryInterface
     */
    private AttributeRepositoryInterface $attributeRepository;

    /**
     * Call parent class and set layout and authentication for this controller and initialize $attributeRepository
     * AttributeController constructor.
     * @param AttributeRepositoryInterface $attributeRepository
     */
    public function __construct(AttributeRepositoryInterface $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
        Auth::check_authenticated();
        parent::__construct();
        $this->view->setLayout(true);
    }

    /**
     * Display list of attributes data
     * GET METHOD
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        $allAttributes = $this->attributeRepository->findAll();
        return $this->view->render($response, 'Attribute.index', ['data' => $allAttributes]);
    }

    /**
     * Display create page of attribute
     * GET METHOD
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function create(ServerRequestInterface $request, ResponseInterface $response)
    {
        $attribute_tables = (object)Config::get('attribute_tables');
        $attribute_type = (object)Config::get('attribute_types');
        return $this->view->render($response, 'Attribute.create', ['data' => ['attribute_tables' => $attribute_tables, 'attributes_types' => $attribute_type]]);
    }

    /**
     * Store attribute to database
     * POST METHOD
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     */
    public function store(ServerRequestInterface $request, ResponseInterface $response)
    {
        $input_data = $request->getParsedBody();
        $rules = [
            'name' => ['required'], 'type' => ['required'], 'table_name' => ['required']
        ];
        $validation = Validator::validate($input_data, $rules);

        if (!$validation->status) {
            Flash::fail($validation->message);
            Redirect::to('/attributes/create');
        }
        $this->attributeRepository->save(new Attribute($input_data));
        Redirect::to('/attributes/index');
    }

    /**
     * Display edit page of attribute
     * GET METHOD
     * @param $id
     * @param $response
     * @return ResponseInterface
     */
    public function edit($id, $response)
    {
        $attribute = $this->attributeRepository->find($id);
        if ($attribute && $attribute instanceof Attribute) {
            $attribute_tables = (object)Config::get('attribute_tables');
            $attribute_type = (object)Config::get('attribute_types');
            return $this->view->render($response, 'Attribute.edit', ['data' => ['attribute' => $attribute, 'attribute_tables' => $attribute_tables, 'attributes_types' => $attribute_type]]);
        }
        Flash::fail('Attribute not found');
        Redirect::to('/attributes/index');
    }

    /**
     * Update attributes entity fields in database
     * POST METHOD
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     */
    public function update(ServerRequestInterface $request, ResponseInterface $response)
    {
        $input_data = $request->getParsedBody();
        $rules = [
            'id' => ['required'], 'name' => ['required'], 'label' => ['required'], 'type' => ['required'], 'table_name' => ['required']
        ];
        $validation = Validator::validate($input_data, $rules);

        if (!$validation->status) {
            Flash::fail($validation->message);
            Redirect::to('/attributes/edit/' . $input_data['id']);
        }
        $this->attributeRepository->update(new Attribute($input_data));

        Redirect::to('/attributes/index');
    }
}