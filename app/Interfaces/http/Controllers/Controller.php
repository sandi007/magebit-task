<?php

namespace App\Interfaces\http\Controllers;

use App\Utility\Session;
use App\Utility\View;

class Controller
{
    protected $view;

    /**
     * Initialize view utility class and session
     * Controller constructor.
     */
    public function __construct()
    {
        $this->view = new View();
        Session::init();
    }
}