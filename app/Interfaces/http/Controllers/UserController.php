<?php


namespace App\Interfaces\http\Controllers;

use App\Application\Service\UserRequestService;
use App\Interfaces\http\Controllers\Controller;
use App\Utility\Auth;
use App\Utility\Flash;
use App\Utility\Redirect;
use App\Utility\Validator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class UserController extends Controller
{
    /**
     * @var UserRequestService
     */
    private UserRequestService $userRequestService;

    /**
     * Call parent class and set layout and authentication for this controller and initialize $userRequestService
     * UserController constructor.
     * @param UserRequestService $userRequestService
     * @throws \Exception
     */
    public function __construct(UserRequestService $userRequestService)
    {
        Auth::check_authenticated();
        $this->userRequestService = $userRequestService;
        parent::__construct();
        $this->view->setLayout(true);
    }


    /**
     * Display list of Users
     * GET METHOD
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws \Exception
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        $all_users = $this->userRequestService->getAllUsers();
        return $this->view->render($response, 'User.index', ['users' => $all_users]);
    }

    /**
     * Display edit user page
     * GET METHOD
     * @param $id
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws \Exception
     */
    public function edit($id, ResponseInterface $response)
    {
        $user_attributes_with_value = $this->userRequestService->getUserWithAttributesValues($id);
        return $this->view->render($response, 'User.edit', ['user_attributes_with_value' => $user_attributes_with_value]);
    }

    /**
     * Update User in database
     * POST METHOD
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @throws \Exception
     */
    public function update(ServerRequestInterface $request, ResponseInterface $response)
    {
        $input_data = $request->getParsedBody();
        $rules = [
            'name' => ['required'],
            'email' => ['required', 'email']
        ];
        $validation = Validator::validate($input_data, $rules);

        if (!$validation->status) {
            Flash::fail($validation->message);
            Redirect::to('/users/edit/' . $input_data['id']);
        }
        $this->userRequestService->updateUserWithAttributesValues($input_data);

        Redirect::to('/users/index');
    }
}