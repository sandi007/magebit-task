<?php


namespace App\Interfaces\http\Controllers;


use App\Interfaces\http\Controllers\Controller;
use App\Utility\Auth;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class HomeController extends Controller
{
    /**
     * Call parent class and set layout and authentication for this controller
     * HomeController constructor.
     */
    public function __construct()
    {
        Auth::check_authenticated();
        parent::__construct();
        $this->view->setLayout(true);
    }

    /**
     * Display Home index page
     * GET METHOD
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws \Exception
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $this->view->render($response, 'Home.index');
    }
}