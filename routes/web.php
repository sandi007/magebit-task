<?php

use App\Interfaces\http\Controllers\AttributeController;
use App\Interfaces\http\Controllers\Auth\LoginController;
use App\Interfaces\http\Controllers\Auth\RegistrationController;
use App\Interfaces\http\Controllers\HomeController;
use App\Interfaces\http\Controllers\UserController;

//Home controller route
$app->get('/', [HomeController::class, 'index']);
$app->get('/index', [HomeController::class, 'index']);

//AuthController route
$app->get('/login', [LoginController::class, 'login']);
$app->post('/login_request', [LoginController::class, 'login_post']);
$app->post('/register_request', [RegistrationController::class, 'register_post']);

//AttributeController route
$app->get('/attributes/index', [AttributeController::class, 'index']);
$app->get('/attributes/create', [AttributeController::class, 'create']);
$app->post('/attributes/store', [AttributeController::class, 'store']);
$app->get('/attributes/edit/{id}', [AttributeController::class, 'edit']);
$app->post('/attributes/update', [AttributeController::class, 'update']);
$app->delete('/attributes/delete', [AttributeController::class, 'delete']);

//UserController
$app->get('/users/index', [UserController::class, 'index']);
$app->get('/users/edit/{id}', [UserController::class, 'edit']);
$app->post('/users/update', [UserController::class, 'update']);
$app->post('/logout', [LoginController::class, 'logout']);