<?php

// All application related configuration

return [
    'SALT' => 'sOYV*3]FE/n+wXX',

    'VIEW' => [
        'PATH' => 'resources/Views/',

        'DEFAULT_LAYOUT_HEADER' => 'Layouts/Default/header.php',

        'DEFAULT_LAYOUT_FOOTER' => 'Layouts/Default/footer.php',

        'LOGIN_LAYOUT_HEADER' => 'Layouts/Login/header.php',

        'LOGIN_LAYOUT_FOOTER' => 'Layouts/Login/footer.php'
    ],
    'SESSION_USER' => 'DkRKETa6QM',

    'BASE_URL' => 'http://localhost/slim-test',

    'BASE_PATH' => '/slim-test',

    'SESSION_FLASH_SUCCESS_KEY' => 'success_flash message',

    'SESSION_FLASH_FAIL_KEY' => 'fail_flash_message'
];