<?php
//All database related configuration
return [
    'MYSQL' => [
        'DEFAULT' => [
            'DB_HOST' => 'localhost',
            'DB_TABLE' => 'Magebit_Task',
            'DB_USERNAME' => 'root',
            'DB_PASSWORD' => ''
        ],
        'PDO' =>
            [
                'DB_CONNECTION_STRING' => 'mysql:host=localhost;dbname=Magebit_Task',
                'DB_USERNAME' => 'root',
                'DB_PASSWORD' => ''
            ]
    ]

];